from telebot import TeleBot
from telebot.storage import StateMemoryStorage
from telebot import custom_filters
from config_data.config import BOT_TOKEN



storage = StateMemoryStorage()
bot = TeleBot(BOT_TOKEN, state_storage=storage)
bot.add_custom_filter(custom_filters.StateFilter(bot))

