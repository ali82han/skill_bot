from states.states import BaseStates
from api.api import get_all_categories
from loader import bot

@bot.message_handler(state=BaseStates.category)
def category_handler(m):
    if m.text not in get_all_categories():
        bot.send_message(m.chat.id, 'Вы указали не верную категорию')
        return
    with bot.retrieve_data(m.from_user.id, m.chat.id) as data:
        data['category'] = m.text
    bot.send_message(m.chat.id, f'Вы выбрали категорию: {m.text}. Укажите кол-во товаров.')
    bot.set_state(m.from_user.id, BaseStates.amount, m.chat.id)
