from loader import bot


# Эхо хендлер, куда летят текстовые сообщения без указанного состояния
@bot.message_handler(state=None)
def bot_echo(message):
    bot.reply_to(
        message, "Я не знаю такой команды. Используйте /help для справки по командам"
    )
